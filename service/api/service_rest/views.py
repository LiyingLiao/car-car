from common.json import ModelEncoder
from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from service_rest.models import ServiceAppointment, Technician, AutomobileVO

# Create your views here.
class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = ["id", "name", "employee_number"]


class ServiceAppointmentListEncoder(ModelEncoder):
    model = ServiceAppointment
    properties = [
        "id",
        "vin",
        "customer_name",
        "date",
        "time",
        "reason",
        "is_completed",
        "is_VIP",
    ]

    def get_extra_data(self, o):
        return {"technician": o.technician.name}

class ServiceAppointmentDetailEncoder(ModelEncoder):
    model = ServiceAppointment
    properties = [
        "id",
        "vin",
        "customer_name",
        "date",
        "time",
        "reason",
        "technician",
        "is_completed",
        "is_VIP",
    ]

    encoders = {
        "technician": TechnicianEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        service_appointments = ServiceAppointment.objects.all()
        return JsonResponse(
            {"service_appointments": service_appointments},
            encoder=ServiceAppointmentListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            technician_number = content["technician"]
            technician = Technician.objects.get(employee_number=technician_number)
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Technician Employee Number"},
                status=400,
            )
       
        try:
            vin = content["vin"]
            car = AutomobileVO.objects.get(vin=vin)
            content["is_VIP"] = True
        except AutomobileVO.DoesNotExist:
            print("This customer is not a VIP")

        content["is_completed"] = False
        service_appointment = ServiceAppointment.objects.create(**content)
        return JsonResponse(
            service_appointment,
            encoder=ServiceAppointmentDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_appointment(request, pk):
    if request.method == "GET":
        try:
            service_appointment = ServiceAppointment.objects.get(id=pk)
            return JsonResponse(
                service_appointment,
                encoder=ServiceAppointmentDetailEncoder,
                safe=False,
            ) 
        except ServiceAppointment.DoesNotExist:
            return JsonResponse({"message": "Invalid Service Appointment ID"}, status=400)
    elif request.method == "DELETE":
        try:
            count, _ = ServiceAppointment.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except ServiceAppointment.DoesNotExist:
            return JsonResponse({"message": "Invalid Service Appointment ID"}, status=400)
    else:
        content = json.loads(request.body)
        try:
            if "technician" in content:
                technician = Technician.objects.get(employee_number=content["technician"])
                content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Technician Number"},
                status=400,
            )
        ServiceAppointment.objects.filter(id=pk).update(**content)
        service_appointment = ServiceAppointment.objects.get(id=pk)
        return JsonResponse(
            service_appointment,
            encoder=ServiceAppointmentDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False,
        )