from django.db import models
from django.urls import reverse

# Create your models here.
class AutomobileVO(models.Model):
    vin = models.CharField(max_length=200, unique=True)
    import_href = models.CharField(max_length=200, unique=True)


class Technician(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.CharField(max_length=100, unique=True)

    def get_api_url(self):
        return reverse("api_technician", kwargs={"pk": self.id})


class ServiceAppointment(models.Model):
    customer_name = models.CharField(max_length=100)
    date = models.DateTimeField()
    time = models.CharField(max_length=100)
    reason = models.CharField(max_length=200)
    technician = models.ForeignKey(
        Technician,
        related_name="service_appointment",
        on_delete=models.CASCADE,
    )
    vin = models.CharField(max_length=200)
    is_completed = models.BooleanField(default=False)
    is_VIP = models.BooleanField(default=False)

    def get_api_url(self):
        return reverse("api_service_appointment", kwargs={"pk": self.id})