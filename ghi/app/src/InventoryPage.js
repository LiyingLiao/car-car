import { NavLink } from "react-router-dom";

function InventoryPage() {
  const autoPic =
    "https://upload.wikimedia.org/wikipedia/commons/thumb/8/84/Audi_R8_black_1.jpg/640px-Audi_R8_black_1.jpg";
  const manuPic1 =
    "  https://upload.wikimedia.org/wikipedia/commons/thumb/4/4e/Leading_car%2C_Audi_Sport_quattro_S1%2C_Audi_S1_EKS_RX_quattro_-1_%2834871295866%29.jpg/640px-Leading_car%2C_Audi_Sport_quattro_S1%2C_Audi_S1_EKS_RX_quattro_-1_%2834871295866%29.jpg";
  const vehiclePic =
    "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7a/Audi_R8_gas_trap.jpg/640px-Audi_R8_gas_trap.jpg";
  return (
    <div className="card-group my-5">
      <div className="card mx-1 border text-center">
        <img src={manuPic1} className="card-img-top" alt="..." />
        <div className="card-body">
          <h5 className="card-title">Manufacturers</h5>
          <p className="card-text">
            Lists all manufacturers that CarCar has in addition to a form that
            can add more.
          </p>
          <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
            <NavLink
              to="/manufacturers/"
              className="btn btn-primary btn-sm px-4 gap-3 mt-3"
            >
              Go to manufacturers
            </NavLink>
            <NavLink
              to="/manufacturers/new/"
              className="btn btn-primary btn-sm px-4 gap-3 mt-3"
            >
              Add a manufacturer
            </NavLink>
          </div>
        </div>
      </div>
      <div className="card mx-1 border text-center">
        <img src={vehiclePic} className="card-img-top" alt="..." />
        <div className="card-body">
          <h5 className="card-title">Vehicle models</h5>
          <p className="card-text">
            Lists all vehicle models that CarCar has in addition to a form to
            add more.
          </p>
          <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
            <NavLink
              to="/models/"
              className="btn btn-primary btn-sm px-4 gap-3"
            >
              Go to vehicle models
            </NavLink>
            <NavLink
              to="/models/new/"
              className="btn btn-primary btn-sm px-4 gap-3"
            >
              Create a new vehicle model
            </NavLink>
          </div>
        </div>
      </div>
      <div className="card mx-1 border text-center">
        <img src={autoPic} className="card-img-top" alt="..." />
        <div className="card-body">
          <h5 className="card-title">Automobiles</h5>
          <p className="card-text">
            Lists all automobiles that CarCar has in addition to a form to add
            more.
          </p>
          <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
            <NavLink
              to="/automobiles/"
              className="btn btn-primary btn-sm px-4 gap-3 mt-5"
            >
              Go to automobiles
            </NavLink>
            <NavLink
              to="/automobiles/new/"
              className="btn btn-primary btn-sm px-4 gap-3 mt-5"
            >
              Add an automobile
            </NavLink>
          </div>
        </div>
      </div>
    </div>
  );
}

export default InventoryPage;
