import React from "react";
import { NavLink } from "react-router-dom";

class EmployeeList extends React.Component {
  constructor(props) {
    super(props);
    this.state = { sales_persons: [] };
  }

  async componentDidMount() {
    const response = await fetch("http://localhost:8090/api/salesteam/");
    if (response.ok) {
      const data = await response.json();
      this.setState({ sales_persons: data.sales_person });
    }
  }

  render() {
    return (
      <>
        <h1>Employees</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>Employee number</th>
            </tr>
          </thead>
          <tbody>
            {this.state.sales_persons.map((employee) => {
              return (
                <tr key={employee.id}>
                  <td>{employee.name}</td>
                  <td>{employee.employee_number}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <div className="d-grid gap-2 d-sm-flex justify-content-sm-left">
          <NavLink
            to="/employees/new/"
            className="btn btn-primary btn-lg px-4 gap-3"
          >
            Add an employee
          </NavLink>
        </div>
      </>
    );
  }
}

export default EmployeeList;
