import React from "react";
import { NavLink } from "react-router-dom";

class SalesList extends React.Component {
  constructor(props) {
    super(props);
    this.state = { sales: [] };
  }

  async componentDidMount() {
    const response = await fetch("http://localhost:8090/api/sales/");
    if (response.ok) {
      const data = await response.json();
      this.setState({ sales: data.sales });
    }
  }

  // too time consuming to work how I want it to, not within project scope
  // async deleteSale(sale) {
  //   const url = `http://localhost:8090/api/sales/${sale.id}/`;
  //   const fetchConfig = {
  //     method: "DELETE",
  //     headers: {
  //       "Content-Type": "application/json",
  //     },
  //   };
  //   const deleteResponse = await fetch(url, fetchConfig);
  //   if (deleteResponse.ok) {
  //     const reload = await fetch("http://localhost:8090/api/sales/");
  //     if (reload.ok) {
  //       const newData = await reload.json();
  //       this.setState({ sales: newData.sales });
  //     }
  //   }
  // }

  render() {
    const format = {
      display: "flex",
      flexdirection: "row",
      justifyContent: "left",
    };
    const cardStyle = {
      marginLeft: "25px",
    };
    return (
      <>
        <h1>Sales history</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Sales person</th>
              <th>Employee number</th>
              <th>Customer</th>
              <th>VIN</th>
              <th>Sale price</th>
            </tr>
          </thead>
          <tbody>
            {this.state.sales.map((sale) => {
              return (
                <tr key={sale.id}>
                  <td>{sale.sales_person.name}</td>
                  <td>{sale.sales_person.employee_number}</td>
                  <td>{sale.customer.name}</td>
                  <td>{sale.automobile.vin}</td>
                  <td>${sale.sale_price}.00</td>
                  {/* <td align="right">
                    <button
                      onClick={() => this.deleteSale(sale)}
                      className="btn btn-danger"
                    >
                      Delete
                    </button>
                  </td> */}
                </tr>
              );
            })}
          </tbody>
        </table>
        <div style={format}>
          <div className="d-grid gap-2 d-sm-flex justify-content-sm-left">
            <NavLink
              to="/sales/new/"
              className="btn btn-primary btn-lg px-4 gap-3"
            >
              Add a sale
            </NavLink>
          </div>
          <div
            style={cardStyle}
            className="d-grid gap-2 d-sm-flex justify-content-sm-center"
          >
            <NavLink
              to="/sales/history/"
              className="btn btn-primary btn-lg px-4 gap-3"
            >
              Sales per employee
            </NavLink>
          </div>
        </div>
      </>
    );
  }
}

export default SalesList;
