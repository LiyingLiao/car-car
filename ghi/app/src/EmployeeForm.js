import React from "react";

class EmployeeForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { name: "", employee_number: "" };
    this.handleChangeName = this.handleChangeName.bind(this);
    this.handleChangeNumber = this.handleChangeNumber.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    const url = "http://localhost:8090/api/salesteam/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "content-Type": "application/json",
      },
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      const cleared = { name: "", employee_number: "" };
      this.setState(cleared);
    }
  }

  handleChangeName(event) {
    this.setState({ name: event.target.value });
  }

  handleChangeNumber(event) {
    this.setState({ employee_number: event.target.value });
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a new employee</h1>
            <form onSubmit={this.handleSubmit} id="create-employee-form">
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleChangeName}
                  value={this.state.name}
                  placeholder="Name"
                  required
                  type="text"
                  name="name"
                  id="name"
                  className="form-control"
                />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleChangeNumber}
                  value={this.state.employee_number}
                  placeholder="Employee number"
                  required
                  type="text"
                  name="employee_number"
                  id="employee_number"
                  className="form-control"
                />
                <label htmlFor="employee_number">
                  Enter an employee number
                </label>
              </div>
              <div align="left">
                <button className="btn btn-primary">Create</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default EmployeeForm;
