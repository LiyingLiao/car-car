import React, { useState, useEffect } from "react";

function ServiceAppointmentSearch() {
    const [appointments, setAppointments] = useState();
    const [vin, setVIN] = useState('');
    
    // Set filteredAppointments to empty list initially so no appointment is shown on the page.
    const [filteredAppointments, setFilteredAppointments] = useState([]); 

    useEffect(() => {
      async function fetchData() {
        const response = await fetch('http://localhost:8080/api/appointments/');
        if (response.ok) {
          const data = await response.json();
          setAppointments(data.service_appointments);
        }
      }
      fetchData();
    }, [])

    function handleSubmit(event) {
        event.preventDefault();
        setFilteredAppointments(appointments.filter((appointment) => appointment.vin === vin));
    }
    
    return (
      <>
        <form className="my-3" onSubmit={(event) => handleSubmit(event)} id="search-appointment-form">
            <div className="input-group mb-3">
                <input onChange={(event) => setVIN(event.target.value)} placeholder="Enter VIN here" required type="text" name="vin" id="vin" className="form-control" />
                <div className="input-group-append">
                    <button className="btn btn-outline-secondary">Search VIN</button>
                </div>
            </div>
        </form>
        <div>
            <h1>Service Appointments</h1>
        </div>
        <table className="table table-striped my-3">
          <thead>
            <tr>
              <th>VIN</th>
              <th>Customer Name</th>
              <th>Date</th>
              <th>Time</th>
              <th>Technician</th>
              <th>Reason</th>
              <th>Finished</th>
              <th>VIP</th>
            </tr>
          </thead>
          <tbody>
            {filteredAppointments && filteredAppointments.map(appointment => {
              return (
                <tr key={appointment.id}>
                  <td>{ appointment.vin }</td>
                  <td>{ appointment.customer_name}</td>
                  <td>{ appointment.date.slice(0,10) }</td>
                  <td>{ appointment.time }</td>
                  <td>{ appointment.technician }</td>
                  <td>{ appointment.reason }</td>
                  <td>{ appointment.is_completed ? 'Yes' : 'No' }</td>
                  <td>{ appointment.is_VIP ? 'VIP' : '' }</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </>
    );
  }
  
  export default ServiceAppointmentSearch;