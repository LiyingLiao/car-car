import { BrowserRouter, Routes, Route } from "react-router-dom";
import InventoryPage from "./InventoryPage";
import MainPage from "./MainPage";
import ManufacturerForm from "./ManufacturerForm";
import ManufacturerList from "./ManufacturerList";
import VehicleModelForm from "./VehicleModelForm";
import VehicleModelsList from "./VehicleModelsList"
import Nav from "./Nav";
import SalesMain from "./SalesMain";
import SalesList from "./SaleRecordsList";
import SaleRecordForm from "./SaleRecordForm";
import EmployeeSaleHistory from "./EmployeeSaleHistory";
import EmployeeList from "./EmployeeList";
import EmployeeForm from "./EmployeeForm";
import CustomerList from "./CustomerList";
import CustomerForm from "./CustomerForm";
import AutomobileList from "./AutosList";
import AutoForm from './AutoForm';
import TechnicianForm from './TechnicianForm';
import ServiceAppointmentForm from './ServiceAppointmentForm';
import ServiceAppointmentsList from './ServiceAppointmentsList';
import ServiceAppointmentSearch from './ServiceAppointmentSearch';
import ServiceMain from './ServiceMain';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="models">
            <Route index element={<VehicleModelsList />} />
            <Route path="new" element={<VehicleModelForm />} />
          </Route>
          <Route path="technicians">
            <Route path="new" element={<TechnicianForm />} />
          </Route>
          <Route path="service">
            <Route index element={<ServiceMain />} />
            <Route path="list" element={<ServiceAppointmentsList />} />
            <Route path="new" element={<ServiceAppointmentForm />} />
            <Route path="search" element={<ServiceAppointmentSearch />} />
          </Route>
          <Route path="inventory" element={<InventoryPage />} />
          <Route path="manufacturers">
            <Route index element={<ManufacturerList />} />
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path="automobiles">
            <Route index element={<AutomobileList />} />
            <Route path="new" element={<AutoForm />} />
          </Route>
          <Route path="sales">
            <Route index element={<SalesMain />} />
            <Route path="list" element={<SalesList />} />
            <Route path="new" element={<SaleRecordForm />} />
            <Route path="history" element={<EmployeeSaleHistory />} />
          </Route>
          <Route path="employees">
            <Route index element={<EmployeeList />} />
            <Route path="new" element={<EmployeeForm />} />
          </Route>
          <Route path="customers">
            <Route index element={<CustomerList />} />
            <Route path="new" element={<CustomerForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;


