import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

 function ServiceAppointmentsList() {
    const [appointments, setAppointments] = useState([]);

    useEffect(() => {
        async function fetchData() {
          const response = await fetch('http://localhost:8080/api/appointments/');
          if (response.ok) {
            const data = await response.json();
            setAppointments(data.service_appointments);
          }
        }
        fetchData();
    }, [])

    async function handleDelete(id) {
        const url = `http://localhost:8080/api/appointments/${id}`;
        const options = {
          method: 'delete'
        }
        const response = await fetch(url, options);
        if (response.ok) {
          const deleted = await response.json();
          console.log(deleted);
        }

        const newAppointments = appointments.filter((appointment) => appointment.id !== id);
        setAppointments(newAppointments);
    }

    async function handleFinish(id) {
        const url = `http://localhost:8080/api/appointments/${id}/`;
        const options = {
          method: 'put',
          body: JSON.stringify({is_completed: true}),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(url, options);
        if (response.ok) {
          const updated = await response.json();
          console.log(updated);
        }

        const newAppointments = appointments.filter((appointment) => appointment.id !== id);
        setAppointments(newAppointments);
    }

    return (
      <>
        <div className="my-3">
            <h1>Service Appointments</h1>
        </div>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>VIN</th>
              <th>Customer Name</th>
              <th>Date</th>
              <th>Time</th>
              <th>Technician</th>
              <th>Reason</th>
              <th>VIP</th>
            </tr>
          </thead>
          <tbody>
            {appointments.filter((appointment) => !appointment.is_completed).map(appointment => {
              return (
                <tr key={appointment.id}>
                  <td>{ appointment.vin }</td>
                  <td>{ appointment.customer_name}</td>
                  <td>{ appointment.date.slice(0,10) }</td>
                  <td>{ appointment.time }</td>
                  <td>{ appointment.technician }</td>
                  <td>{ appointment.reason }</td>
                  <td>{appointment.is_VIP ? 'VIP' : ''}</td>
                  <td><button onClick={() => handleDelete(appointment.id)} type="button" className="btn btn-danger btn-sm">Cancel</button></td>
                  <td><button onClick={() => handleFinish(appointment.id)} type="button" className="btn btn-success btn-sm">Finished</button></td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <div className="d-grid gap-2 d-sm-flex justify-content-sm-center my-4">
            <Link to="/service/new" className="btn btn-success btn-lg px-4 gap-3">Create a new appointment</Link>
            <Link to="/service/search" className="btn btn-success btn-lg px-4 gap-3">Search history by VIN</Link>
        </div>
      </>
    );
  }
  
  export default ServiceAppointmentsList;