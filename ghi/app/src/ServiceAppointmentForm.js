import React from 'react';
import { Link } from 'react-router-dom';

class ServiceAppointmentForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            vin: '',
            customer_name: '',
            date: '',
            time: '',
            reason: '',
            technicians: [],
        }

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleVINChange = this.handleVINChange.bind(this);
        this.handleCustomerNameChange = this.handleCustomerNameChange.bind(this);
        this.handleDateChange = this.handleDateChange.bind(this);
        this.handleTimeChange = this.handleTimeChange.bind(this);
        this.handleReasonChange = this.handleReasonChange.bind(this);
        this.handleTechnicianChange = this.handleTechnicianChange.bind(this);
    }

  async componentDidMount() {
    const url = 'http://localhost:8080/api/technicians/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();
        this.setState({ technicians: data.technicians });
    }
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    delete data.technicians;

    const url = 'http://localhost:8080/api/appointments/';
    const fetchOptions = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      }
    };
    const appointmentResponse = await fetch(url, fetchOptions);
    if (appointmentResponse.ok) {
      this.setState({
        vin: '',
        customer_name: '',
        date: '',
        time: '',
        reason: '',
        technician: '',
      });
    }
  }

  handleVINChange(event) {
    const value = event.target.value;
    this.setState({ vin: value });
  }

  handleCustomerNameChange(event) {
    const value = event.target.value;
    this.setState({ customer_name: value });
  }

  handleDateChange(event) {
    const value = event.target.value;
    this.setState({ date: value });
  }

  handleTimeChange(event) {
    const value = event.target.value;
    this.setState({ time: value });
  }

  handleReasonChange(event) {
    const value = event.target.value;
    this.setState({ reason: value });
  }
  
  handleTechnicianChange(event) {
    const value = event.target.value;
    this.setState({ technician: value });
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a service appointment</h1>
            <form onSubmit={this.handleSubmit} id="create-service-appointment-form">
              <div className="form-floating mb-3">
                <input value={this.state.vin} onChange={this.handleVINChange} placeholder="vin" required type="text" name="vin" id="vin" className="form-control" />
                <label htmlFor="vin">VIN</label>
              </div>
              <div className="form-floating mb-3">
                <input value={this.state.customer_name} onChange={this.handleCustomerNameChange} placeholder="Customer Name" required type="text" name="customer_name" id="customer_name" className="form-control" />
                <label htmlFor="customer_name">Customer Name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={this.state.date} onChange={this.handleDateChange} placeholder="date" required type="text" name="date" id="date" className="form-control" />
                <label htmlFor="date">Date</label>
              </div>
              <div className="form-floating mb-3">
                <input value={this.state.time} onChange={this.handleTimeChange} placeholder="time" required type="text" name="time" id="time" className="form-control" />
                <label htmlFor="time">Time</label>
              </div>
              <div className="form-floating mb-3">
                <input value={this.state.reason} onChange={this.handleReasonChange} placeholder="reason" required type="text" name="reason" id="reason" className="form-control" />
                <label htmlFor="reason">Reason</label>
              </div>
              <div className="mb-3">
                <select value={this.state.technician} onChange={this.handleTechnicianChange} required name="technician" id="technician" className="form-select">
                  <option value="">Choose a technician</option>
                  {this.state.technicians.map(technician => {
                    return (
                      <option key={technician.id} value={technician.employee_number}>
                        {technician.name}(Employee Number: {technician.employee_number})
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
          <div className="d-grid gap-2 d-sm-flex justify-content-sm-center my-5">
            <Link to="/service/list" className="btn btn-success btn-lg px-4 gap-3">Go back to appointments list</Link>
          </div>
        </div>
      </div> 
    );
  }
}

export default ServiceAppointmentForm;