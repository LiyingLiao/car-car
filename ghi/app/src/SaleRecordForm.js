import React from "react";

class SaleRecordForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      automobile: "",
      automobiles: [],
      sales_person: "",
      sales_persons: [],
      customer: "",
      customers: [],
      sale_price: "",
    };
    this.handleChangeAuto = this.handleChangeAuto.bind(this);
    this.handleChangeEmploy = this.handleChangeEmploy.bind(this);
    this.handleChangeCusto = this.handleChangeCusto.bind(this);
    this.handleChangePrice = this.handleChangePrice.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async componentDidMount() {
    // automobiles dropdown
    const autoUrl = "http://localhost:8100/api/automobiles/";
    const autoResponse = await fetch(autoUrl);
    const autoData = await autoResponse.json();
    // employee dropdown
    const employUrl = "http://localhost:8090/api/salesteam/";
    const employResponse = await fetch(employUrl);
    const employData = await employResponse.json();
    // customer dropdown
    const custoUrl = "http://localhost:8090/api/customers/";
    const custoResponse = await fetch(custoUrl);
    const custoData = await custoResponse.json();
    this.setState({
      automobiles: autoData.autos,
      sales_persons: employData.sales_person,
      customers: custoData.customers,
    });
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    // only want to send selected values
    delete data.automobiles;
    delete data.sales_persons;
    delete data.customers;
    //
    const saleUrl = "http://localhost:8090/api/sales/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(saleUrl, fetchConfig);
    if (response.ok) {
      const newSale = await response.json();
      this.updateSold(newSale);
      const cleared = {
        automobile: "",
        sales_person: "",
        customer: "",
        sale_price: "",
      };
      this.setState(cleared);
    }
  }

  // updates the auto inventory to sold
  async updateSold(auto) {
    const soldUrl = `http://localhost:8100${auto.automobile.import_href}`;
    const fetchUpdate = {
      method: "put",
      body: JSON.stringify({ sold: true }),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const updateResponse = await fetch(soldUrl, fetchUpdate);
    if (updateResponse.ok) {
      console.log("holy shit it worked");
    }
  }

  handleChangeAuto(event) {
    this.setState({ automobile: event.target.value });
  }

  handleChangeEmploy(event) {
    this.setState({ sales_person: event.target.value });
  }

  handleChangeCusto(event) {
    this.setState({ customer: event.target.value });
  }

  handleChangePrice(event) {
    this.setState({ sale_price: event.target.value });
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Record a sale</h1>
            <form onSubmit={this.handleSubmit} id="create-sale-form">
              <div className="mb-3">
                <select
                  onChange={this.handleChangeAuto}
                  value={this.state.automobile}
                  required
                  name="automobile"
                  id="automobile"
                  className="form-select"
                >
                  <option value="">Select an automobile</option>
                  {this.state.automobiles
                    .filter((automobile) => automobile.sold === false)
                    .map((automobile) => {
                      return (
                        <option key={automobile.id} value={automobile.vin}>
                          {automobile.vin}
                        </option>
                      );
                    })}
                </select>
              </div>
              <div className="mb-3">
                <select
                  onChange={this.handleChangeEmploy}
                  value={this.state.sales_person}
                  required
                  name="sales_person"
                  id="sales_person"
                  className="form-select"
                >
                  <option value="">Select a sales person</option>
                  {this.state.sales_persons.map((sales_person) => {
                    return (
                      <option
                        key={sales_person.id}
                        value={sales_person.employee_number}
                      >
                        {sales_person.name} - {sales_person.employee_number}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className="mb-3">
                <select
                  onChange={this.handleChangeCusto}
                  value={this.state.customer}
                  required
                  name="customer"
                  id="customer"
                  className="form-select"
                >
                  <option value="">Select a customer</option>
                  {this.state.customers.map((customer) => {
                    return (
                      <option key={customer.id} value={customer.id}>
                        {customer.name} - {customer.phone_number}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleChangePrice}
                  value={this.state.sale_price}
                  placeholder="Sale price"
                  required
                  type="number"
                  name="sale_price"
                  id="sale_price"
                  className="form-control"
                />
                <label htmlFor="sale_price">Sale price</label>
              </div>
              <div align="left">
                <button className="btn btn-primary">Create</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default SaleRecordForm;
