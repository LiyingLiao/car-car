import { Link } from 'react-router-dom';
import React, { useState, useEffect } from 'react';

function VehicleModelsList() {
  const [models, setModels] = useState([])

  useEffect(() => {
    async function fetchData() {
      const response = await fetch('http://localhost:8100/api/models/');
      if (response.ok) {
        const data = await response.json();
        setModels(data.models);
      }
    }
    fetchData();
  }, [])
  
  
  return (
    <>
      <div className="my-3">
        <h1>Vehicle Models</h1>
      </div>
      <table className="table table-striped my-2">
        <thead>
          <tr>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Picture</th>
          </tr>
        </thead>
        <tbody>
          {models.map(model => {
            return (
              <tr key={model.id}>
                <td>{ model.name }</td>
                <td>{ model.manufacturer.name }</td>
                <td><img src={ model.picture_url } alt="" width="250" height="150"/></td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <div className="d-grid gap-2 d-sm-flex justify-content-sm-center my-4">
        <Link to="/models/new" className="btn btn-success btn-lg px-4 gap-3">Create a new vehicle model</Link>
      </div>
    </>
  );
  }
  
  export default VehicleModelsList;
  