import React from "react";
import { NavLink } from "react-router-dom";

class CustomerList extends React.Component {
  constructor(props) {
    super(props);
    this.state = { customers: [] };
  }

  async componentDidMount() {
    const response = await fetch("http://localhost:8090/api/customers/");
    if (response.ok) {
      const data = await response.json();
      this.setState({ customers: data.customers });
    }
  }

  render() {
    return (
      <>
        <h1>Customers</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>Phone number</th>
              <th>Address</th>
            </tr>
          </thead>
          <tbody>
            {this.state.customers.map((customer) => {
              return (
                <tr key={customer.id}>
                  <td>{customer.name}</td>
                  <td>{customer.phone_number}</td>
                  <td>{customer.address}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <div className="d-grid gap-2 d-sm-flex justify-content-sm-left">
          <NavLink
            to="/customers/new/"
            className="btn btn-primary btn-lg px-4 gap-3"
          >
            Add a customer
          </NavLink>
        </div>
      </>
    );
  }
}

export default CustomerList;
