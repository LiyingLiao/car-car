import { NavLink } from "react-router-dom";

function ServiceMain() {
  const pic1 =
    "https://images.unsplash.com/photo-1593699199342-59b40e08f0ac?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80";
  const pic2 =
    "https://images.unsplash.com/photo-1522241112606-b5d35a468795?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80";
  return (
    <div className="card-group my-5">
      <div className="card mx-4 border text-center">
        <img src={pic2} className="card-img-top" alt="..." />
        <div className="card-body">
          <h5 className="card-title">Appointments</h5>
          <p className="card-text">
            Lists all appointments with an option to add more or search appointments by vin 
          </p>
          <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
            <NavLink
              to="/service/list/"
              className="btn btn-success btn-sm px-4 gap-3"
            >
              Appointments List
            </NavLink>
            <NavLink
              to="/service/new/"
              className="btn btn-success btn-sm px-4 gap-3"
            >
              Add an Appointment
            </NavLink>
            <NavLink
              to="/service/search/"
              className="btn btn-success btn-sm px-4 gap-3"
            >
              Search History By VIN
            </NavLink>
          </div>
        </div>
      </div>
      <div className="card mx-4 border text-center">
        <img src={pic1} className="card-img-top" alt="..." />
        <div className="card-body">
          <h5 className="card-title">Technicians</h5>
          <p className="card-text">Add a new technician to the system </p>
          <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
            <NavLink
              to="/technicians/new/"
              className="btn btn-success btn-lg px-4 gap-3 mt-4"
            >
              Add a Technician
            </NavLink>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ServiceMain;

