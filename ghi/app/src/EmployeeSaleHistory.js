import React from "react";

class EmployeeSaleHistory extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sales_persons: [],
      sale_history: [],
      sales_person: "",
    };
    this.handleCheckSales = this.handleCheckSales.bind(this);
  }

  async componentDidMount() {
    const employUrl = "http://localhost:8090/api/salesteam/";
    const employResponse = await fetch(employUrl);
    const employData = await employResponse.json();
    this.setState({ sales_persons: employData.sales_person });
  }

  // gets sales made by employee
  async handleCheckSales(event) {
    const employID = event.target.value;
    if (employID.length > 0) {
      this.setState({ sales_person: employID });
      const historyUrl = `http://localhost:8090/api/salesteam/sale/${employID}/`;
      const historyResponse = await fetch(historyUrl);
      if (historyResponse.ok) {
        const historyData = await historyResponse.json();
        this.setState({ sale_history: historyData.employees_sales });
      }
    }
  }

  render() {
    let tableContent;
    const historyData = this.state.sale_history;
    if (historyData.length > 0) {
      tableContent = (
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Sales person</th>
              <th>Customer</th>
              <th>VIN</th>
              <th>Sale price</th>
            </tr>
          </thead>
          <tbody>
            {this.state.sale_history.map((sale) => {
              return (
                <tr key={sale.id}>
                  <td>{sale.sales_person}</td>
                  <td>{sale.customer}</td>
                  <td>{sale.VIN}</td>
                  <td>${sale.sale_price}.00</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      );
    } else {
      tableContent = (
        <div className="alert alert-info" role="alert">
          <p style={{ textAlign: "center", fontWeight: "bold" }}>
            No sales yet
          </p>
        </div>
      );
    }
    return (
      <>
        <h1>Employee sale history</h1>
        <div className="mb-3">
          <select
            onChange={this.handleCheckSales}
            required
            name="sales_person"
            id="sales_person"
            className="form-select"
          >
            <option value="">Select a sales person</option>
            {this.state.sales_persons.map((sales_person) => {
              return (
                <option key={sales_person.id} value={sales_person.id}>
                  {sales_person.name} - {sales_person.employee_number}
                </option>
              );
            })}
          </select>
        </div>
        {tableContent}
      </>
    );
  }
}

export default EmployeeSaleHistory;
