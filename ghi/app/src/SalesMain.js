import { NavLink } from "react-router-dom";

function SalesMain() {
  const employPic =
    "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2a/Workforce_NOW_education_panel_%2837362256141%29.jpg/640px-Workforce_NOW_education_panel_%2837362256141%29.jpg";
  const custoPic =
    "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/Stack_of_100_dollar_bills.jpg/640px-Stack_of_100_dollar_bills.jpg";
  const salesPic =
    "https://upload.wikimedia.org/wikipedia/commons/thumb/4/41/PriceGoesUp.png/640px-PriceGoesUp.png";
  return (
    <div className="card-group my-5">
      <div className="card mx-1 border text-center">
        <img src={salesPic} className="card-img-top" alt="..." />
        <div className="card-body">
          <h5 className="card-title">Sale Records</h5>
          <p className="card-text">
            Lists all sales with an option to add more or view sales made by
            each employee
          </p>
          <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
            <NavLink
              to="/sales/list/"
              className="btn btn-primary btn-sm px-4 gap-3"
            >
              Sale records
            </NavLink>
            <NavLink
              to="/sales/new/"
              className="btn btn-primary btn-sm px-4 gap-3"
            >
              New sale
            </NavLink>
            <NavLink
              to="/sales/history/"
              className="btn btn-primary btn-sm px-4 gap-3"
            >
              Sales by employee
            </NavLink>
          </div>
        </div>
      </div>
      <div className="card mx-4 border text-center">
        <img src={employPic} className="card-img-top" alt="..." />
        <div className="card-body">
          <h5 className="card-title">Employees</h5>
          <p className="card-text">
            View all employees or add a new employee and assign an employee
            number.
          </p>
          <div className="d-grid gap-2 py-4 d-sm-flex justify-content-sm-center">
            <NavLink
              to="/employees/"
              className="btn btn-primary btn-md px-4 gap-3"
            >
              Go to employees
            </NavLink>
            <NavLink
              to="/employees/new/"
              className="btn btn-primary btn-md px-4 gap-3"
            >
              Add an employee
            </NavLink>
          </div>
        </div>
      </div>
      <div className="card mx-4 border text-center">
        <img src={custoPic} className="card-img-top" alt="..." />
        <div className="card-body">
          <h5 className="card-title">Customers</h5>
          <p className="card-text">View all customers or add a new customer.</p>
          <div className="d-grid gap-2 py-4 d-sm-flex justify-content-sm-center">
            <NavLink
              to="/customers/"
              className="btn btn-primary btn-md px-4 gap-3"
            >
              Go to customers
            </NavLink>
            <NavLink
              to="/customers/new/"
              className="btn btn-primary btn-md px-4 gap-3"
            >
              Add a customer
            </NavLink>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SalesMain;
