# CarCar

CarCar is for car dealerships that are looking to do the following utilizing RESTful APIs and microservice architecture with Docker:

- view/update available inventory
- view/add sales and sale records
- view/manage employees
- view/add customers
- view/manage service appointments
- view/add technicians

Team:

- Liying Liao - service
- Malik - Sales

## Design

https://excalidraw.com/#json=zRbT1iWg3gwEKrqX1hY_E,l3J23x5zCEHnojpLfzwpGA

## Service microservice

This microservice is defined within the bounded context of service, which mainly is made up of two parts, the technician and the service appointment. There is also a Value Object AutomobileVO which is connected to an entity in another microservice called Inventory by a poller.

This microservice used Django for backend and React for front-end.

---

### **Models**

This microservice has three models in total, two "native" models(Technician and ServiceAppointment) and one imported value object model(AutomobileVO).

---

Technician - A technical person who works for CarCar and performs service.

```bash
    {
        "name": string
        "employee_number": string, unique
    }
```

- The APIs for Technician model are GET and POST.
- Only the creation form developed for the front-end.

---

ServiceAppointment -

```bash
    {
        "customer_name": string
        "date": DateTime
        "time": string
        "reason": string
        "technician": a Foreign Key connecting to Technician model through employee number
        "vin": string
        "is_completed": boolean, default to false, can be altered on the appointments list
        "is_VIP": boolean, default to false, reassigned upon appointments creation by comparing VINs in appointments with VINs in AutomobileVO
    }
```

- The APIs for ServiceAppointments are GET and POST for list and GET/PUT/DELETE for individual.
- A list of appointments are available in front-end, in the list, user can mark "cancel" or "finish" to an appointment.
- A creation form to create a new appointment.
- A search history page, default to display no records. Displays records match the VIN entered in the search bar.

---

AutomobileVO - the value object created mirroring Automobile in Inventory microservice

```bash
    {
        "vin": the unique value that used to make connection
        "import_href": href kept for reference
    }
```

- No APIs or front-end created in this microservice.

---

### **Integration**

Since this microservice is talking to a different entity within another bounded context, we use poller to fetch data. In ServiceAppointment model, the boolean field **is_VIP** is determined by whether the appointment VIN once appeared in the Automobile model. So to achieve this connection, I created AutomobileVO model, collecting essential info from Automobile model to fulfil the checking task. During the creation of a new appointment(POST request), the backend checks if the VIN in form equals any VINs in the AutomobileVO database, if so, is_VIP changed to true from its default value false. The poller runs every 60s.

---

## Sales microservice

## Models

**AutomobileVO:** Value object of the Automobile model in the inventory-api. The vin, import href, and whether or not its sold are polled for in poller.py every 60s.

**SalesPerson:** A person who works for CarCar, this model takes in:

- name (string)
- employee number (string)

**Customer:** Represents a customer who will purchase an automobile, takes in:

- name (string)
- address (string)
- phone number (string, unique)

**SaleRecord:** This allows the user to document a sale made by the dealership, a sale made will also remove the automobile purchased from available inventory. This is not accessed through the value object, but handled in the frontend with a request to the inventory-api. This model takes in:

- sale price (integer)
- automobile (string, vin of automobile and ForeignKey to AutomobileVO)
- sales person (string, employee number and ForeignKey to SalesPerson)
- customer id (id of customer from database and ForeignKey to Customer)

## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/MalikOmar/project-beta.git
```

Go to the project directory

```bash
  cd project-beta
```

Create volume to store data

```bash
  docker volume create beta-data
```

Build the docker images

```bash
  docker-compose build
```

Start docker containers

```bash
  docker-compose up
```

To see CarCar in browser, http://localhost:3000
