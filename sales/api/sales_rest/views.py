import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .encoders import SalesPersonEncoder, CustomerEncoder, SaleRecordEncoder, SaleHistoryEncoder
from .models import SalesPerson, Customer, SaleRecord, AutomobileVO


#<------ SalesPerson views ------>
@require_http_methods(["GET", "POST"])
def api_list_sales_person(request):
    if request.method == "GET":
        sales_person = SalesPerson.objects.all()
        return JsonResponse({"sales_person": sales_person}, encoder=SalesPersonEncoder)
    else: # POST
        content = json.loads(request.body)
        try:
            sales_person = SalesPerson.objects.create(**content)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False
            )
        except:
            return JsonResponse({"message": "Could not create sales person"}, status=400)


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_sales_person(request, code):
    if request.method == "GET":
        employee = SalesPerson.objects.get(employee_number=code)
        return JsonResponse(
            employee,
            encoder=SalesPersonEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        try:
            SalesPerson.objects.filter(employee_number=code).delete()
            employee = SalesPerson.objects.get(employee_number=code)
            return JsonResponse(
                employee,
                encoder=SalesPersonEncoder,
                safe=False
            )
        except SalesPerson.DoesNotExist:
            return JsonResponse({"message": "Does not exist"}, status=400)
    else: # PUT
        content = json.loads(request.body)
        try:
            SalesPerson.objects.filter(employee_number=code).update(**content)
            employee = SalesPerson.objects.get(employee_number=code)
            return JsonResponse(
                employee,
                encoder=SalesPersonEncoder,
                safe=False
            )
        except SalesPerson.DoesNotExist:
            return JsonResponse({"message": "Does not exist"}, status=400)



#<------ Customer views ------>
@require_http_methods(["GET", "POST"])
def api_customers(request):
    if request.method == "GET":
        customer = Customer.objects.all()
        return JsonResponse(
            {"customers": customer},
            encoder=CustomerEncoder,
            safe=False
        )   
    else: # POST
        content = json.loads(request.body)
        try:
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False
            )
        except:
            return JsonResponse({"message": "Could not create customer"}, status=400)


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_customer(request, pk):
    if request.method == "GET":
        customer = Customer.objects.get(id=pk)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        try:
            Customer.objects.filter(id=pk).delete()
            customer = Customer.objects.get(id=pk)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Does not exist"}, status=400)
    else: # PUT
        content = json.loads(request.body)
        try:
            Customer.objects.filter(id=pk).update(**content)
            customer = Customer.objects.get(id=pk)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Does not exist"}, status=400)


#<------ SaleRecord views ------>
@require_http_methods(["GET", "POST"])
def api_list_sale_record(request):
    if request.method == "GET":
        sales = SaleRecord.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleRecordEncoder,
            safe=False
        )
    else: # POST
        content = json.loads(request.body)
        try:
            content["sales_person"] = SalesPerson.objects.get(employee_number=content["sales_person"])
            content["automobile"] = AutomobileVO.objects.get(vin=content["automobile"])
            content["customer"] = Customer.objects.get(id=content["customer"])
            sale = SaleRecord.objects.create(**content)
            return JsonResponse(
                sale,
                encoder=SaleRecordEncoder,
                safe=False
            )
        except:
            return JsonResponse({"message": "Could not create sale record"}, status=400)


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_sale_record(request, pk):
    if request.method == "GET":
        sale = SaleRecord.objects.get(id=pk)
        return JsonResponse(
            sale,
            encoder=SaleRecordEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        try:
            SaleRecord.objects.filter(id=pk).delete()
            sale = SaleRecord.objects.get(id=pk)
            return JsonResponse(
                sale,
                encoder=SaleRecordEncoder,
                safe=False
            )
        except SaleRecord.DoesNotExist:
            return JsonResponse({"message": "Does not exist"}, status=400)
    else: # PUT updates by ID, remember this
        content = json.loads(request.body)
        try:
            SaleRecord.objects.filter(id=pk).update(**content)
            sale = SaleRecord.objects.get(id=pk)
            return JsonResponse(
                sale,
                encoder=SaleRecordEncoder,
                safe=False
            )
        except SaleRecord.DoesNotExist:
            return JsonResponse({"message": "Does not exist"}, status=400)


#<------ Sales by employee ------>
@require_http_methods(["GET"])
def api_show_sale_by_person(request, pk):
    if request.method == "GET":
        try:
            employee_sale = SaleRecord.objects.filter(sales_person_id=pk)
            return JsonResponse(
                {"employees_sales": employee_sale},
                encoder=SaleHistoryEncoder,
                safe=False
            )
        except SaleRecord.DoesNotExist:
            return JsonResponse({"message": "Does not exist"}, status=400)
