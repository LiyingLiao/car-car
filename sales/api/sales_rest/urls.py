from django.urls import path
from .views import (
    api_list_sales_person,
    api_show_sales_person,
    api_customers,
    api_show_customer,
    api_list_sale_record,
    api_show_sale_record,
    api_show_sale_by_person,
    )


urlpatterns = [
    path("salesteam/", api_list_sales_person, name="api_create_sales_person"),
    path("salesteam/<str:code>/", api_show_sales_person, name="api_show_sales"),
    path("customers/", api_customers, name="api_create_customers"),
    path("customers/<int:pk>/", api_show_customer, name="api_show_customer"),
    path("sales/", api_list_sale_record, name="api_create_sale_record"),
    path("sales/<int:pk>/", api_show_sale_record, name="api_show_sale"),
    path("salesteam/sale/<int:pk>/", api_show_sale_by_person, name="api_show_sale_by_person"),
]
