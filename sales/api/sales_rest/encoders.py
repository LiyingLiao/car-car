from common.json import ModelEncoder
from .models import Customer, SalesPerson, SaleRecord, AutomobileVO


class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "id",
        "name",
        "employee_number",
    ]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "name",
        "address",
        "phone_number",
    ]


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "id",
        "import_href",
        "sold",
        "vin"
    ]


class SaleRecordEncoder(ModelEncoder):
    model = SaleRecord
    properties = [
        "id",
        "sale_price",
        "sales_person",
        "customer",
        "automobile",
    ]
    encoders = {
        "sales_person": SalesPersonEncoder(),
        "customer": CustomerEncoder(),
        "automobile": AutomobileVOEncoder(),
    }


class SaleHistoryEncoder(ModelEncoder):
    model = SaleRecord
    properties = [
        "id",
        "sale_price"
    ]

    def get_extra_data(self, o):
        return {
            "sales_person": o.sales_person.name,
            "customer": o.customer.name,
            "VIN": o.automobile.vin
        }