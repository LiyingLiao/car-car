from django.db import models
from django.urls import reverse

# Create your models here.
class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=125, unique=True)
    sold = models.BooleanField(default=False)
    

class SalesPerson(models.Model):
    name = models.CharField(max_length=150)
    employee_number = models.CharField(max_length=100, null=True, unique=True)

    # def get_api_url(self):
    #     return reverse("api_show_sales", kwargs={"pk": self.pk})

    class Meta:
        ordering = ["employee_number"]


class Customer(models.Model):
    name = models.CharField(max_length=150)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=20, unique=True)

    class Meta:
        ordering = ["name"]


class SaleRecord(models.Model):
    sale_price = models.IntegerField() # could be negative (refund)

    sales_person = models.ForeignKey(
        SalesPerson,
        related_name="sale_record",
        on_delete=models.PROTECT,
        null=True
    )

    customer = models.ForeignKey(
        Customer,
        related_name="sale_record",
        on_delete=models.PROTECT
    )

    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sale_record",
        on_delete=models.PROTECT
    )